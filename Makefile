.SUFFIXES: .mli .cmi .ml .cmo .cmx

OCAMLC ?= ocamlc
OCAMLOPT ?= ocamlopt
OCAMLDEP ?= ocamldep

OCAMLFLAGS += -safe-string -w -30-40

SRCDIR = src

COMMANDS = ablctl ablmeter

BINS = $(COMMANDS:%=%.run)
ifneq (,$(OCAMLOPT))
ifneq (,$(shell $(OCAMLOPT) -v 2>/dev/null))
BINS += $(COMMANDS:%=%.opt)
endif
endif

.PHONY: all
all: $(BINS)

# Dependencies
$(SRCDIR)/.depend:
	$(OCAMLDEP) -I $(SRCDIR) $(SRCDIR)/*.mli $(SRCDIR)/*.ml >$(SRCDIR)/.depend

# Executables

# ablctl

ABLCTL_MODULES = ablctl abl log modbus tty util
ABLCTL_PACKS = unix

ABLCTL_SRCS = $(shell $(OCAMLDEP) -all -sort $(ABLCTL_MODULES:%=$(SRCDIR)/%.ml))

ablctl.run: $(ABLCTL_SRCS:%.ml=%.cmo)
	$(OCAMLC) $(OCAMLFLAGS) -I $(SRCDIR) -o $@ - $(ABLCTL_PACKS:%=%.cma) $+
ablctl.opt: $(ABLCTL_SRCS:%.ml=%.cmx)
	$(OCAMLOPT) $(OCAMLFLAGS) -I $(SRCDIR) -o $@ - $(ABLCTL_PACKS:%=%.cmxa) $+


# ablmeter

ABLMETER_MODULES = ablmeter meter abl log modbus tty util
ABLMETER_PACKS = unix

ABLMETER_SRCS = $(shell $(OCAMLDEP) -all -sort $(ABLMETER_MODULES:%=$(SRCDIR)/%.ml))

ablmeter.run: $(ABLMETER_SRCS:%.ml=%.cmo)
	$(OCAMLC) $(OCAMLFLAGS) -I $(SRCDIR) -o $@ - $(ABLMETER_PACKS:%=%.cma) $+
ablmeter.opt: $(ABLMETER_SRCS:%.ml=%.cmx)
	$(OCAMLOPT) $(OCAMLFLAGS) -I $(SRCDIR) -o $@ - $(ABLMETER_PACKS:%=%.cmxa) $+


.mli.cmi .ml.cmi:
	$(OCAMLC) $(OCAMLFLAGS) -I $(SRCDIR) -c - $<

.ml.cmx:
	$(OCAMLOPT) $(OCAMLFLAGS) -I $(SRCDIR) -c - $<

.ml.cmo:
	$(OCAMLC) $(OCAMLFLAGS) -I $(SRCDIR) -c - $<

.PHONY: clean
clean:
	$(RM) $(SRCDIR)/*.cmi $(SRCDIR)/*.cmx $(SRCDIR)/*.cmo $(SRCDIR)/*.o
	$(RM) $(SRCDIR)/.depend
	$(RM) $(COMMANDS:%=%.run) $(COMMANDS:%=%.opt)

include src/.depend
