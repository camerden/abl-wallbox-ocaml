exception Modbus_error of string
exception Modbus_slave_error of string
exception Modbus_length_error of string
exception Modbus_opcode_error of string
exception Modbus_timeout

type dev = {
    fd: Unix.file_descr;
    addr: int;
  }

type op = Read | Write

type msg = {
    addr: int;
    op: op;
    register: int;
    len: int;
    value: bytes option;
  }
and resp = {
    addr: int;
    op: op;
    len: int;
    msg: string;
  }


let int_of_hex s = int_of_string ("0x" ^ s)

let twos8 i = (i lxor 0xFF + 1) land 0xFF

let bytes_of_int i =
  let rec f i acc =
    match i with
    | i when i > 0 -> f (i lsr 8) ([(i mod 256) |> Char.chr] @ acc)
    | i -> acc
  in
  let l = f i [] in
  Bytes.init (List.length l) (fun i -> List.nth l i)

let bytes_fixlen len bytes =
  let diff = len - (Bytes.length bytes) in
  match diff with
  | d when d > 0 -> Bytes.cat (Bytes.make diff '\x00') bytes
  | d when d < 0 -> Bytes.extend bytes d 0
  | d -> bytes


let int_of_op = function
  | Read -> 0x03
  | Write -> 0x10

let op_of_int = function
  | 0x03 -> Read
  | 0x10 -> Write
  | _ -> failwith "invalid modbus opcode"


let lrc (b: bytes) : int =
  let l = Util.list_of_bytes b |> List.map Char.code in
  let lsum = List.fold_left (fun a b -> (a + b) land 0xFF) 0 l in
  twos8 lsum

let lrc_check s lrc =
  let l = Util.unasciify s |> Util.list_of_bytes |> List.map Char.code in
  let lsum = List.fold_left (fun a b -> (a + b) land 0xFF) 0 l in
  ((lsum + lrc) land 0xFF) = 0



let recv_line (fd: Unix.file_descr) =
  let line = Tty.read_line_fd_nonblock ~timeout:1. fd in
  Log.debug "received line: %s%!" line;
  match String.trim line with
  | "" -> raise (Modbus_error "no response")
  | s when String.length s >= 3 && s.[0] = '>' ->
     let resp = String.sub s 1 (String.length s - 3) in

     (* Check LRC of response message *)
     let lrc_is = int_of_hex @@ String.sub s (String.length s - 2) 2 in
     if not (lrc_check resp lrc_is) then
       raise (Modbus_error (Printf.sprintf "Invalid LRC in response: %s (is: %02X, but should be: %02X)"
             s lrc_is (lrc @@ Util.unasciify resp)));

     (* Return response without LRC *)
     resp
  | s -> raise (Modbus_error ("Invalid response: " ^ s))

let line_of_msg (msg : msg) = (* no line delimiters *)
  let bytes =
    Bytes.concat Bytes.empty [
        (bytes_of_int msg.addr |> bytes_fixlen 1);
        (int_of_op msg.op |> Char.chr |> Bytes.make 1);
        (bytes_of_int msg.register |> bytes_fixlen 2);
        (bytes_of_int msg.len |> bytes_fixlen 2);
        (match msg.value with
         | Some b ->
             Bytes.cat (Bytes.length b |> bytes_of_int |> bytes_fixlen 1) b
         | None -> Bytes.empty
         )
      ]
  in
  Printf.sprintf ":%s%02X" (Util.asciify bytes) (lrc bytes)

let run_request (dev : dev) (req : msg) : resp =
  let parse_response line =
    Scanf.sscanf line "%02X%02X%02X%s" (fun slave_id opcode msglen msg ->
       (* Check slave ID *)
       if slave_id <> req.addr then
         raise (Modbus_slave_error (Printf.sprintf "Invalid slave ID in response (is: %u, expected: %u)" slave_id req.addr));

       (* Check op code (MSB is 1 on error) *)
       let opcode_exp = int_of_op req.op in
       if opcode <> opcode_exp then
         raise (Modbus_opcode_error (Printf.sprintf "Invalid op code in response (is: %u, expected: %u)" opcode opcode_exp));

       (* for write requests the address and length of the written data will be returned *)
       let msg_data =
         match op_of_int opcode with
         | Read -> msg
         | Write -> Scanf.sscanf msg "%02X%04X%s" (fun addr len msg -> msg)
       in

       (* Check length of response message *)
       let msglen_is = (String.length msg_data) / 2 in
       if msglen_is <> msglen then
         raise (Modbus_length_error (Printf.sprintf "Incorrect response message length (is: %u, expected: %u)" msglen_is msglen));

       (* Return the data part of the response *)
       Log.debug "Reveived response from slave %u to op 0x%02X request, length: %u\n%!" slave_id opcode msglen;
       { addr = slave_id; op = op_of_int opcode; len = msglen; msg = msg_data })
  in
  let req_line = line_of_msg req in
  let rec retry i =
    if i < 1 then
      failwith "retries exhausted";
    try
      Tty.send_line dev.fd req_line;
      recv_line dev.fd
    with
    | End_of_file | Modbus_slave_error _ | Modbus_opcode_error _ ->
       retry i
    | Sys_blocked_io | Modbus_timeout | Modbus_length_error _ | Modbus_error _  ->
       Log.debug "retries left: %u\n%!" (i-1);
       retry (i-1)
  in
  let resp = retry 5 in
  parse_response resp
