let () =
  (* default values for command-line arguments *)
  let devname = ref ""
  and devaddr = ref 1
  and meter_session_file = ref ""
  and t_delta = ref 600
  in

  (* command line argument processing *)
  Arg.parse [
    ("-dev", Arg.Set_string devname, "the device");
    ("-i", Arg.Set_int devaddr, Printf.sprintf "the device address (default %u)" !devaddr);
    ("-tdelta", Arg.Set_int t_delta, Printf.sprintf "the time delta that triggers a new entry in the session file (default %us)" !t_delta);
    ("-debug", Arg.Set Log.debug_mode, "enable debug output");
    ("-m", Arg.Set_string meter_session_file, "meter session with state file");
  ] (fun (s) -> ()) "usage";
  if !devname = "" then
    raise (Arg.Bad "-dev is required");
  if !meter_session_file = "" then
    raise (Arg.Bad "no meter session file (-m) given");
  if !t_delta <= 0 then
    raise (Arg.Bad "-tdelta must not be > 0");

  let wb : Modbus.dev = {
      fd = Tty.openfile !devname;
      addr = !devaddr;
    }
  in
  try
    Tty.lock wb.fd;
    Tty.setup wb.fd;
    Abl.wakeup_bus ~tries:3 wb;
    Tty.unlock wb.fd;

    let meter_session =
      Meter.start wb !meter_session_file {
          line_delta_t = !t_delta;
        }
    in
    begin
      try
        Meter.loop meter_session;
        Meter.stop meter_session;
      with e ->
        Meter.stop meter_session;
        raise e
    end;

    Tty.close wb.fd
  with e ->
    Tty.unlock wb.fd;
    Tty.close wb.fd;
    raise e
