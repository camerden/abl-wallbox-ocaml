open Log

type 'a abl_command = {
    request: Modbus.msg;
    response_handler: (Modbus.resp -> 'a);
    to_string: ('a -> string);
  }

type wb_state = A1 | B1 | B2 | C2 | E0 (* TODO: complete *)

type evcc2_current = {
    ict1: float;
    ict2: float;
    ict3: float;
  }
and evcc2_systemflags = {
    f4: bool;
    b1b2onBoot: bool;
    bc3: bool;
    bc4: bool;
    bc5: bool;
    bc6: bool;
    b1b2possible: bool;
    overCurrent100: bool;
    overCurrent110: bool;
    overCurrent120: bool;
  }
and evcc2_fwversion = {
    pcb: evcc2_pcb;
    fw_version: string;
  }
and evcc2_lid = {
    enabled: bool;
    detected: bool;
    i_lid: int;
  }
and evcc2_pcb = PCB_141215 | PCB_160307 | PCB_170725
and evcc2_pwm = {
    lid: evcc2_lid;
    i_rated: int;
    i_default: int;
    i_c_max: int;
    i_c: int;
  }

let evcc2_pcb_of_int = function
  | 0 -> PCB_141215
  | 1 -> PCB_160307
  | 2 -> PCB_170725
  | _ -> failwith "unknown PCB code"

let string_of_evcc2_pcb = function
  | PCB_141215 -> "PCB_141215"
  | PCB_160307 -> "PCB_160307"
  | PCB_170725 -> "PCB_170725"

let wb_state_of_string = function
  | "A1" -> A1
  | "B1" -> B1
  | "B2" -> B2
  | "C2" -> C2
  | "E0" -> E0
  | s -> failwith (Printf.sprintf "%s: Unknown wallbox state" s)

let string_of_wb_state = function
  | A1 -> "A1"
  | B1 -> "B1"
  | B2 -> "B2"
  | C2 -> "C2"
  | E0 -> "E0"


let round (f : float) : int =
  floor (f +. 0.5) |> truncate

let test_bit (n : int) (i : Int64.t) : bool =
  Int64.(logand i (shift_left 1L n)) <> 0L

let i_to_pwm_perc = function
  | i when i >= 6 && i <= 51 ->
      (float_of_int i) /. 0.6
  | i when i > 51 && i <= 80 ->
      (float_of_int i) /. 2.5 +. 64.
  | i -> 0.

type abl_commands = {
    current: (Modbus.dev -> evcc2_current abl_command);
    deviceid: (Modbus.dev -> int abl_command);
    fwversion: (Modbus.dev -> evcc2_fwversion abl_command);
    getpwm: (Modbus.dev -> evcc2_pwm abl_command);
    interrupt: (Modbus.dev -> unit abl_command);
    lock: (Modbus.dev -> unit abl_command);
    reset: (Modbus.dev -> unit abl_command);
    setpwm: (int -> Modbus.dev -> unit abl_command);
    state: (Modbus.dev -> wb_state abl_command);
    systemflags: (Modbus.dev -> evcc2_systemflags abl_command);
    unlock: (Modbus.dev -> unit abl_command);
  }

let commands = {
    current =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Read;
            register = 0x2E;
            len = 0x5;
            value = None;
          };
          response_handler =
            (fun resp ->
              Scanf.sscanf resp.msg "%08X%04X%04X%04X" (fun _ ph1 ph2 ph3 ->
                  let transform_ph = function
                    | 0x03E8 -> 0. (* no vehicle connected *)
                    | 0x0001 -> 0. (* vehicle not charging *)
                    | ph -> (float_of_int ph) /. 10.
                  in
                  { ict1 = transform_ph ph1;
                    ict2 = transform_ph ph2;
                    ict3 = transform_ph ph3;
                  }
                )
            );
          to_string =
            (fun r ->
              Printf.sprintf "L1 = %.1f A; L2 = %.1f A; L3 = %.1f A" r.ict1 r.ict2 r.ict3
            );
        }
      );
    deviceid =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Read;
            (* register = 0x50;
             * len = 0x8; *)
            register = 0x2C;
            len = 0x1;
            value = None;
          };
          response_handler = (fun resp -> int_of_string resp.msg);
          to_string = (fun devid -> string_of_int devid);
      });
    fwversion =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Read;
            register = 0x0001;
            len = 0x0002;
            value = None;
          };
          response_handler =
            (fun resp ->
              Scanf.sscanf resp.msg "%02X%02X%01X%01X%02X" (fun _ pcb_code fw_vmajor fw_vminor _ ->
                  { pcb = evcc2_pcb_of_int (pcb_code lsr 6 land 3);
                    fw_version = Printf.sprintf "%u.%u" fw_vmajor fw_vminor
                  }
                )
            );
          to_string =
            (fun r ->
              Printf.sprintf "%s, FW v%s" (string_of_evcc2_pcb r.pcb) r.fw_version
            )
        }
      );
    getpwm =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Read;
            register = 0x000F;
            len = 0x0005;
            value = None;
          };
          response_handler =
            (fun resp ->
              Scanf.sscanf resp.msg "%02X%02LX%04X%04X%04X%04X" (fun _ lid_byte i_rated i_default i_c_max i_c ->
                  { lid = {
                      enabled = test_bit 6 lid_byte;
                      detected = test_bit 7 lid_byte;
                      i_lid = Int64.to_int (Int64.logand lid_byte 0x0FFFL);
                    };
                    i_rated = round ((float_of_int i_rated) *. 0.06);
                    i_default = round ((float_of_int i_default) *. 0.06);
                    i_c_max = round ((float_of_int i_c_max) *. 0.06);
                    i_c = round ((float_of_int i_c) *. 0.06);
                  }
            ));
          to_string =
            (fun r ->
              Printf.sprintf "LID enabled = %s; LID detected = %s; LID = %03X\nrated: %u A; default: %u A; c_max: %u A; c: %u A"
                (if r.lid.enabled then "true" else "false")
                (if r.lid.detected then "true" else "false")
                r.lid.i_lid
                r.i_rated
                r.i_default
                r.i_c_max
                r.i_c
            )
        }
      );
    interrupt =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Write;
            register = 0x14;
            len = 0x1;
            value = Some (Bytes.of_string "\x03\xE8");
          };
          response_handler = (fun resp -> ());
          to_string = (fun _ -> "");
        }
      );
    lock =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Write;
            register = 0x5;
            len = 0x1;
            value = Some (Bytes.of_string "\xE0\xE0");
          };
          response_handler = (fun resp -> ());
          to_string = (fun _ -> "");
        }
      );
    reset =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Write;
            register = 0x5;
            len = 0x1;
            value = Some (Bytes.of_string "\x5A\x5A");
          };
          response_handler = (fun resp -> ());
          to_string = (fun _ -> "");
        }
      );
    setpwm =
      (fun (amps : int) wb ->
        { request = {
            addr = wb.addr;
            op = Write;
            register = 0x14;
            len = 0x1;
            value =
              let dperc = (i_to_pwm_perc amps) *. 10. |> round in
              Some Modbus.(bytes_of_int dperc |> bytes_fixlen 2)
          };
          response_handler = (fun resp -> ());
          to_string = (fun _ -> "");
        }
      );
    state =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Read;
            register = 0x4;
            len = 0x1;
            value = None;
          };
          response_handler =
            (fun resp ->
              String.sub resp.msg 2 2 |> wb_state_of_string
            );
          to_string =
            function
            | A1 -> "A1: Waiting for connection"
            | B1 -> "B1: Connected, waiting for charging"
            | B2 -> "B2: Ready to charge"
            | C2 -> "C2: Charging...";
            | E0 -> "E0: Outlet locked";
        }
      );
    systemflags =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Read;
            register = 0x6;
            len = 0x2;
            value = None;
          };
          response_handler =
            (fun resp ->
              let bitfield = Int64.of_string ("0x" ^ resp.msg) in
              { f4 = test_bit 15 bitfield;
                b1b2onBoot = not (test_bit 14 bitfield);
                bc3 = test_bit 8 bitfield;
                bc4 = test_bit 6 bitfield;
                bc5 = test_bit 7 bitfield;
                bc6 = test_bit 9 bitfield;
                b1b2possible = not (test_bit 4 bitfield);
                overCurrent100 = test_bit 0 bitfield;
                overCurrent110 = test_bit 1 bitfield;
                overCurrent120 = test_bit 2 bitfield;
              }
            );
          to_string =
            (fun r ->
              Printf.sprintf "f4: %B, b1b2onBoot: %B, bc3: %B, bc4: %B, bc5: %B, bc6: %B, b1b2possible: %B, overCurrent100: %B, overCurrent110: %B, overCurrent120: %B"
                r.f4 r.b1b2onBoot r.bc3 r.bc4 r.bc5 r.bc6 r.b1b2possible r.overCurrent100 r.overCurrent110 r.overCurrent120);
        }
      );
    unlock =
      (fun wb ->
        { request = {
            addr = wb.addr;
            op = Write;
            register = 0x5;
            len = 0x1;
            value = Some (Bytes.of_string "\xA1\xA1");
          };
          response_handler = (fun resp -> ());
          to_string = (fun _ -> "");
        }
      );
  }


let run_command_by_name_and_serialise (wb : Modbus.dev) (c_str : string) (value : string option) : string =
  match c_str with
  | "current" ->
     let cmd = commands.current wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "deviceid" ->
     let cmd = commands.deviceid wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "fwversion" ->
     let cmd = commands.fwversion wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "getpwm" ->
     let cmd = commands.getpwm wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "interrupt" ->
     let cmd = commands.interrupt wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "lock" ->
     let cmd = commands.lock wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "reset" ->
     let cmd = commands.reset wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "setpwm" ->
     let cmd =
       match value with
       | Some s -> commands.setpwm (int_of_string s) wb
       | None -> failwith "missing value"
     in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "state" ->
     let cmd = commands.state wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "systemflags" ->
     let cmd = commands.systemflags wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | "unlock" ->
     let cmd = commands.unlock wb in
     Modbus.run_request wb cmd.request |> cmd.response_handler |> cmd.to_string
  | c_str -> failwith ("Invalid command: " ^ c_str)

  (* | "output" ->
   *    { request = {
   *        addr = wb.addr;
   *        op = Read;
   *        register = 0x9;
   *        len = 0x2;
   *        value = None;
   *      };
   *      response_handler = fun resp -> Serialised resp.msg;
   *    } *)

  (* | "setid" ->
   *    { request = {
   *        addr = wb.addr;
   *        op = Write;
   *        register = 0x2C;
   *        len = 0x1;
   *        value =
   *          match value with
   *          | Some s -> ???
   *          | None -> failwith "missing value";
   *        ;
   *      };
   *      response_handler = fun resp -> Serialised resp.msg;
   *    } *)

let rec wakeup_bus ?(tries = 3) (wb : Modbus.dev) : unit =
  let cmd = commands.fwversion wb in
  let fwvers = Modbus.run_request wb cmd.request |> cmd.response_handler in
  debug "connected to: %s\n%!" (cmd.to_string fwvers)
