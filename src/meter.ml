type session_config = {
    line_delta_t: int;  (* start a new line at least every n seconds *)
  }

type session_state = {
    wb: Modbus.dev;
    config: session_config;

    start: float;

    (* measurements *)
    mutable coulombs: float * float * float;  (* coulombs metered in the current line *)
    mutable last_update_c: float;  (* last update of coulombs field *)

    mutable wb_state: Abl.wb_state;  (* current wallbox state *)
    mutable last_change_s: float;  (* last change of wallbox state *)
    mutable last_update_s: float;  (* last update/refresh of wallbox state *)

    (* session file *)
    mutable last_write_f: float;  (* last update of session file *)
    mutable last_line_t: float;  (* timestamp when log line was started *)
    state_fd: Unix.file_descr;
  }

let tell fd =
  Unix.(lseek fd 0 SEEK_CUR)

let write_state session =
  let p = ref (tell session.state_fd) in
  let write ?(restore_p=false) ?(seek_end=false) contents =
    let p_prev = !p in
    if seek_end then
      p := Unix.(lseek session.state_fd 0 SEEK_END);
    let bwritten = Unix.single_write_substring session.state_fd contents 0 (String.length contents) in
    if (String.length contents) > bwritten then
      (* restore previous contents *)
      ignore (Unix.(lseek session.state_fd (-bwritten) SEEK_CUR))
    else
      p := !p + bwritten;
    Unix.ftruncate session.state_fd !p;
    if restore_p then
      p := Unix.(lseek session.state_fd p_prev SEEK_SET)
  in
  let gen_line coulombs last_update =
    let (c1, c2, c3) = coulombs in
    Printf.sprintf "%010.3f %010.3f %010.3f %Lu"
      c1 c2 c3 (Int64.of_float last_update)
  in
  if !p < 11 then
    (* write header *)
    begin
      let offset = Unix.(lseek session.state_fd 0 SEEK_SET) in
      if offset > 0 then
        failwith "lseek";
      Printf.sprintf "%Lu\n" (Int64.of_float session.start) |> write
    end;
  let now = Unix.gettimeofday () in
  if (truncate (now -. session.last_line_t) > session.config.line_delta_t) then
    begin
      (* new line *)
      write ~restore_p:false (
          gen_line session.coulombs session.last_update_c ^ "\n");
      session.last_line_t <- now;
      session.last_write_f <- now;
      session.coulombs <- (0., 0., 0.);
      session.last_update_c <- now;
    end;
  write ~restore_p:true (gen_line session.coulombs session.last_update_c);
  session.last_write_f <- now


let wb_command (wb : Modbus.dev) (cmd : 'a Abl.abl_command) : 'a =
  Tty.lock wb.fd;
  try
    let r = Modbus.run_request wb cmd.request |> cmd.response_handler in
    Tty.unlock wb.fd;
    r
  with e ->
    Tty.unlock wb.fd;
    raise e

let current_currents (wb : Modbus.dev) : Abl.evcc2_current =
  Abl.commands.current wb |> wb_command wb

let current_state (wb : Modbus.dev) : Abl.wb_state =
  Abl.commands.state wb |> wb_command wb

let start wb path config =
  let state_fd = Unix.(openfile path [
      O_RDWR; O_CREAT; O_EXCL; O_NOCTTY; O_SYNC] 0o640)
  in
  Unix.(lockf state_fd F_LOCK 0);
  let state =
    let now = Unix.gettimeofday () in
    { wb = wb;
      config = config;
      start = now;
      coulombs = (0., 0., 0.);
      last_update_c = now;
      wb_state = current_state wb;
      last_change_s = now;
      last_update_s = now;
      last_write_f = now;
      last_line_t = now;
      state_fd = state_fd;
    }
  in
  write_state state;
  state

let stop session =
  write_state session;
  let fd = session.state_fd in
  ignore (Unix.(lseek fd 0 SEEK_SET));
  Unix.(lockf fd F_ULOCK 0);
  Unix.close fd

let update session : unit =
  let ict = current_currents session.wb in
  let now = Unix.gettimeofday () in
  let tdelta = now -. session.last_update_c in
  let (c1, c2, c3) = session.coulombs in
  session.coulombs <- (
    c1 +. ict.ict1 *. tdelta,
    c2 +. ict.ict2 *. tdelta,
    c3 +. ict.ict3 *. tdelta
  );
  session.last_update_c <- now;
  if (now -. session.last_update_s) > 10. then
    begin
      (* didn't check wallbox state for more than 1 min *)
      Log.debug "checking wb state\n%!";
      let state = current_state session.wb in
      let now = Unix.gettimeofday () in
      if (state <> session.wb_state) then
        session.last_change_s <- now;
      session.wb_state <- state;
      session.last_update_s <- now
    end

let rec loop session : unit =
  try
    update session;
    if ((session.last_update_c -. session.last_write_f) > 10.) then
      begin
        try
          write_state session
        with e -> ()
      end;
    let now = Unix.gettimeofday () in
    match session.wb_state with
    | Abl.A1 when (now -. session.last_change_s) > 60. ->
       () (* stop session *)
    | _ ->
       begin
         let sleept = max 0.1 (1. -. (now -. session.last_update_c)) in
         Log.debug "sleeping for %f s\n%!" sleept;
         Unix.sleepf sleept;
         loop session
       end
  with e ->
    (* retry *)
    loop session
