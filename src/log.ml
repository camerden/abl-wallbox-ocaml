let debug_mode = ref false

let debug format =
  (if !debug_mode then Printf.fprintf else Printf.ifprintf) stderr format
