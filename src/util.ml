let list_of_bytes b =
  let r = ref [] in
  for i = ((Bytes.length b) - 1) downto 0 do
    r := (Bytes.get b i) :: !r
  done;
  !r

let rec list_of_hexstring = function
  | "" -> []
  | s -> Scanf.sscanf s "%02X%s" (fun c s -> c :: list_of_hexstring s)

let asciify bytes =
  list_of_bytes bytes
  |> List.fold_left (fun s b -> s ^ (Printf.sprintf "%02X" (Char.code b))) ""

let unasciify s =
  let il = list_of_hexstring s in
  Bytes.init (List.length il) (fun i -> List.nth il i |> Char.chr)
