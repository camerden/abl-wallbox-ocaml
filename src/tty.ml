open Log

let openfile path =
  debug "opening tty: %s\n%!" path;
  Unix.openfile path [
      Unix.O_RDWR; Unix.O_NOCTTY; Unix.O_NONBLOCK; Unix.O_SYNC] 0

let close fd =
  Unix.close fd

let lock fd =
  let locked = ref false
  and n_try = ref 0 in
  while not !locked do
    try
      incr n_try;
      Unix.(lockf fd F_TLOCK 0);
      locked := true;
      if !n_try > 1 then
        debug "\n%!"
    with
    | Unix.(Unix_error (EAGAIN, _, _))
    | Unix.(Unix_error (EACCES, _, _)) ->
       if !n_try = 1 then
         debug "waiting for lock%!";
       debug ".%!";
       Unix.sleepf 0.01
  done

let unlock fd =
  let pos = Unix.(lseek fd 0 SEEK_CUR) in
  ignore (Unix.(lseek fd 0 SEEK_SET));
  Unix.(lockf fd F_ULOCK 0);
  ignore (Unix.(lseek fd pos SEEK_SET))

let setup fd =
  debug "setting up tty...\n%!";
  let curr_settings = Unix.tcgetattr fd in
  let abl_settings =
    { curr_settings with
      Unix.c_ibaud = (300 lsl 7);
      Unix.c_obaud = (300 lsl 7);
      Unix.c_csize = 8;
      Unix.c_cstopb = 1;
      Unix.c_parenb = true;
      Unix.c_parodd = false;
      Unix.c_inpck = true;
      Unix.c_hupcl = false;
      Unix.c_ixon = false;
      Unix.c_ixoff = false;
      Unix.c_opost = false;
      Unix.c_isig = false;
      Unix.c_icanon = false;
      Unix.c_echo = false;
    } in
  if curr_settings <> abl_settings then
    Unix.tcflush fd Unix.TCIOFLUSH;
    Unix.tcsetattr fd Unix.TCSANOW abl_settings;
    Unix.sleepf 0.1


let send_line (fd: Unix.file_descr) (line: string) : unit =
  debug "sending line: %s%!" line;
  let b = Bytes.of_string (line ^ "\r\n") in
  ignore (Unix.single_write fd b 0 (Bytes.length b));
  Unix.tcdrain fd;
  debug "\n%!"

let read_line_fd_nonblock ?(timeout = 1.) fd =
  let size_incr = 64 in
  Log.debug "Reading: %!";
  let _buf = ref (Bytes.create size_incr)
  and _pos = ref 0
  in
  let rec _scan_endline timeout =
    let start = Unix.gettimeofday () in
    try
      while !_pos < 1 || (Bytes.get !_buf (!_pos-1)) <> '\n' do
        (* Read until either EOF or fd blocks *)
        if ((Bytes.length !_buf) - !_pos) < 1 then
          (* extend buffer by 64 characters, expected lines are short *)
          _buf := Bytes.extend !_buf 0 size_incr;
        match Unix.read fd !_buf !_pos 1 with
        | 0 -> raise End_of_file
        | n ->
          Log.debug "%s%!" (Bytes.to_string @@ Bytes.sub !_buf !_pos n);
          _pos := !_pos + n
      done;
      Bytes.sub !_buf 0 !_pos
    with
    | End_of_file
    | Sys_blocked_io
    | Unix.(Unix_error (EAGAIN, _, _))
    | Unix.(Unix_error (EWOULDBLOCK, _, _)) ->
       begin
         let elapsed = Unix.gettimeofday () -. start in
         let rest = timeout -. elapsed in
         if rest <= 0. then
           begin
             (* timeout reached *)
             Log.debug "\n%!";
             raise Sys_blocked_io
           end;
         match Unix.select [fd] [] [] rest with
         | fdl, _, _ when List.mem fd fdl ->
            _scan_endline rest
         | _, _, _ ->
            Log.debug "\n%!";
            raise Sys_blocked_io
       end
  in
  _scan_endline timeout |> Bytes.to_string

let rec read_line_fd ?(timeout = 1.) fd : string =
  let in_ch = Unix.in_channel_of_descr fd
  and start = Unix.gettimeofday() in
  try
    input_line in_ch
  with
  | Sys_blocked_io ->
     begin
       Unix.sleepf 0.01;
       let elapsed = Unix.gettimeofday() -. start in
       let rest = timeout -. elapsed in
       if rest <= 0. then
         (* timeout reached *)
         raise Sys_blocked_io;
       read_line_fd ~timeout:rest fd
     end
  | e -> raise e
