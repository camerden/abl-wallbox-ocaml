let () =
  (* default values for command-line arguments *)
  let devname = ref ""
  and devaddr = ref 1
  and command = ref ""
  and value = ref ""
  in

  (* command line argument processing *)
  Arg.parse [
    ("-dev", Arg.Set_string devname, "the device");
    ("-i", Arg.Set_int devaddr, "the device address (default 1)");
    ("-debug", Arg.Set Log.debug_mode, "enable debug output");
    ("-c", Arg.Set_string command, "command to execute");
    ("-v", Arg.Set_string value, "value to command");
  ] (fun (s) -> ()) "usage";
  if !devname = "" then
    raise (Arg.Bad "-dev is required");
  if !command = "" then
    raise (Arg.Bad "no command (-c) given");

  let wb : Modbus.dev = {
      fd = Tty.openfile !devname;
      addr = !devaddr;
    }
  in
  try
    Tty.lock wb.fd;
    Tty.setup wb.fd;

    let to_option = function
      | v when v <> "" -> Some v
      | _ -> None
    in
    let resp = Abl.run_command_by_name_and_serialise wb !command (to_option !value) in
    if (resp <> "") then
      print_endline resp;

    Tty.unlock wb.fd;
    Tty.close wb.fd
  with e ->
    Tty.unlock wb.fd;
    Tty.close wb.fd;
    raise e
